//importing fs and path modules
const fs = require("fs");
const path = require("path");

//function that perform various tasks of problem 2
function problem2Function() {
  return new Promise((resolve, reject) => {
    //creating the file path for lipsum
    let lipsumFilePath = path.join(__dirname, "lipsum_1.txt");
    //reading the lipsum file
    fs.readFile(lipsumFilePath, "utf-8", (error1, data) => {
      if (error1) {
        reject(error1);
      }

      // console.log("\nlipsum data=", data);
      let uppercaseData = data.toUpperCase();
      let uppercaseFileName = "uppercase.txt";
      //creating the file path for uppercase content
      let uppercaseFilePath = path.join(__dirname, uppercaseFileName);
      //writing the content to uppercase file
      fs.writeFile(uppercaseFilePath, uppercaseData, (error2) => {
        if (error2) {
          reject(error2);
        }

        //adding the uppercase file name to filenames.txt
        fs.appendFile("filenames.txt", uppercaseFileName + "\n", (error3) => {
          if (error3) {
            reject(error3);
          }

          //reading the uppercase file
          fs.readFile(uppercaseFilePath, "utf-8", (error4, data) => {
            if (error4) {
              reject(error4);
            }

            // console.log("\nuppercase data=", data);
            let lowercaseData = data.toLowerCase();
            let splittedData = lowercaseData.split(". ");
            let lowercaseFileName = "lowercase.txt";
            //creating the lowercase file path
            let lowercaseFilePath = path.join(__dirname, lowercaseFileName);
            //writing the content to lowercase file
            fs.writeFile(lowercaseFilePath, splittedData, (error5) => {
              if (error5) {
                reject(error5);
              }

              //adding the lowercase filename to filenames.txt
              fs.appendFile(
                "filenames.txt",
                lowercaseFileName + "\n",
                (error6) => {
                  if (error6) {
                    reject(error6);
                  }

                  //getting the new files
                  let outputFiles = [uppercaseFileName, lowercaseFileName];
                  //sorting the content of those new files
                  let sortedContent = outputFiles
                    .map((file) => {
                      return fs.readFileSync(file, "utf-8");
                    })
                    .sort()
                    .join("\n");
                  // console.log("\nsortedContent=", sortedContent);
                  let sortedFileName = "sorted.txt";
                  let sortedFilePath = path.join(__dirname, sortedFileName);
                  //writing sorted content to sorted file
                  fs.writeFile(sortedFilePath, sortedContent, (error7) => {
                    if (error7) {
                      reject(error7);
                    }

                    //adding the sorted file name to filenames.txt
                    fs.appendFile(
                      "filenames.txt",
                      sortedFileName + "\n",
                      (error8) => {
                        if (error8) {
                          reject(error8);
                        }

                        //reading the filenames from filenames.txt
                        fs.readFile(
                          "filenames.txt",
                          "utf-8",
                          (error9, fileNames) => {
                            if (error9) {
                              reject(error9);
                            }
                            // console.log("\nfileNames=", fileNames);
                            let filesToBeDeleted = fileNames.trim().split("\n");

                            //iterating through each file and deleting those files
                            filesToBeDeleted.forEach((file) => {
                              fs.unlink(file, (error10) => {
                                if (error10) {
                                  reject(error10);
                                }
                              });
                            });

                            //after all operations returning the message
                            resolve("all files deleted");
                          }
                        );
                      }
                    );
                  });
                }
              );
            });
          });
        });
      });
    });
  });
}

//exporting the above function
module.exports = problem2Function;
