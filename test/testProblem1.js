//importing createFilesAndDeleteFiles Function
const createFilesAndDeleteFiles = require("../problem1");

let directoryPath = "./randomFolder";
let noOfFiles = 5;

//executing the createFilesAndDeleteFiles function
async function test() {
  try {
    let message = await createFilesAndDeleteFiles(directoryPath, noOfFiles);
    console.log(message);
  } catch (error) {
    console.log(error);
  }
}

test();
