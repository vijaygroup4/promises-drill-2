//importing problem2 function
let problem2Function = require("../problem2");

//executing the problem2 function
async function test() {
  try {
    let message = await problem2Function();
    console.log(message);
  } catch (error) {
    console.log(error);
  }
}

test();
